# Analyzer Scripts

Scripts for interacting with Secure's Analyzers. Adding this project to the shell `$PATH` will allow
`analyzer-build` or `analyzer-run` to be ran from within analyzer repos.

## Inferred Docker Tag

The included analyzer scripts use a convention for docker tag names. They will generate a name based on the
current working directory name and the branch name. For example, if you are in a clone of gosec on branch
master, then it will use `gosec:master` for the docker tag. It should also be noted that git allows certain
characters in branch names that are not allowed as a part of a docker tag. In those cases, you must specify
the docker tag.

## Directory Structure

The examples below assume a directory structure that mirrors that of the groups in GitLab. For example:

security-products:

```
├── analyzers
│   ├── bandit
│   ├── brakeman
│   ├── common
│   └── ...
└── tests
    ├── c
    ├── python-pipenv
    └── ...
```

## `analyzer-build`

This script will build the docker image and automatically tag it with the inferred docker tag. This is mostly
just a convenience to use a convention for docker tag names when building and running via `analyzer-run`.

Basic example: `analyzer-build`

Example with specified tag: `analyzer-build gosec:w00t`

## `analyzer-run`

This script is a convenience for running an analyzer docker image against a test repo. It will mount the test
project as well as the analyzer inside the docker container. There are a few ways it can be ran.

## `analyzer-test-downstreams`

This script is a convenience for running an analyzer docker image against all downstream test repos. It will
iterate over downstream projects mentioned within `.gitlab-ci.yml`,  mount the test projects, and compare
generated reports against our 
[`compare_reports.sh` script](https://gitlab.com/gitlab-org/security-products/ci-templates/-/blob/master/scripts/compare_reports.sh).
This script does a smart comparison between report fields and verifies the validity against
[security-report-schemas](https://gitlab.com/gitlab-org/security-products/security-report-schemas).

### Volume Mounts

There are two volume mounts that are attached to the running container. The first is the test app get's
mounted at `/tmp/app` and the current working directory is mounted at `/tmp/current_dir`.

##e Simple test run

To simply test the current analyzer run the script with a path to a test project that the analyzer can run
against. The resulting sast report will be saved to the test projects root dir.

```
analyzer-run ../../tests/test_project
```

### Run image and drop into a shell

There are times when you want to be able to investigate how something is ran or try other analyzer commmands
etc. In that case you can run the script with a docker tag and a command to run (e.g. `sh` or `bash`). For
example, here is how you could get a shell with the
[flawfinder](https://gitlab.com/gitlab-org/security-products/analyzers/flawfinder/) analyzer on branch master and
the [c](https://gitlab.com/gitlab-org/security-products/tests/c/) test project.

```
analyzer-run ../../tests/c flawfinder:master bash
```

You can then run the analyzer baked into the image at `/analyzer` or also have access to other files and
binaries in the current working directory.

### Use a go replace directive to use local dependencies

This strategy is useful when you need to test out development of a dependency (e.g. [common](https://gitlab.com/gitlab-org/security-products/analyzers/common/)) using the
[replace directive](https://github.com/golang/go/wiki/Modules#when-should-i-use-the-replace-directive).
It requires two terminals, which will be designated as `1$` and `2$`. It's important that you build the docker
image before updating the go.mod as most of our Dockerfiles use a multi stage build to build the go analyzer
and will not be able to build the go project as your replace directive will be a local path on the host
machine.

- `1$`: Build the docker image (e.g. `analyzer-build`)
- `1$`: Run container with a shell command (e.g. `analyzer-run ../../tests/c flawfinder:master bash`)
- `2$`: Update go.mod to point at a local path for the depencency with the [replace directive](https://github.com/golang/go/wiki/Modules#when-should-i-use-the-replace-directive)
- `2$`: Build the analyzer locally (e.g. `GOOS=linux go build -o analyzer`)
- `1$`: In the container cd to the volume (e.g. `cd /tmp/current_dir`)
- `1$`: Run the analyzer you built on your host machine (e.g. `./analyzer`)


